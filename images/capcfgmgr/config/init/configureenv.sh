#!/bin/bash
#/usr/bin/supervisord -c /etc/supervisord.conf

# Variables from environement
: ${SALT_MASTER:=localhost}
: ${ORCHESTRATOR:=RANCHER}
: ${LOG_LEVEL:=info}
: ${MULE_ENV:=dev}
: ${HOSTNAME:=localhost}
: ${OPTIONS:=}

#Change the Runtime specific components
#    - Salt Minion  : Change the Master to IP Address or DNS of Master
rm -f /etc/salt/minion

echo "INFO: Configuring Grandmaster Minion"
cp /etc/salt/minion-default /etc/salt/minion


if [ $ORCHESTRATOR = "RANCHER" ]; then
    #stackname=$(curl -s rancher-metadata/latest/self/stack/name)
    #sed -i 's/master: 127.0.0.1/master: '"$stackname"'/' /etc/salt/minion
    # Set minion id
    echo configmgr-$HOSTNAME > /etc/salt/minion_id
else
    echo "INFO: No Orchestrator. Can only be used for confirming if Container starts properly"
    export MULE_GRPID=grndmstrminion
    export HOSTNAME=localhost
    mkdir -p /opt/mule/apps    
fi
    
