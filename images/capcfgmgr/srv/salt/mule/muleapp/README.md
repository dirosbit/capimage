Placeholder for Apps to be deployed. Hierarchy to be followed:

muleapp
  |---<buone>
  |    |----dev
  |    |      |---app1.zip
  |    |      |---app2.zip
  |---<butwo>
  |    |----stage
  |    |      |---app1.zip
  |    |      |---app2.zip  
  |---<buthree>
       |---app1-dev.zip
       |---app1-stage.zip    
       
Use Tags or Folder structure to deploy the Apps to the respective Containers