# vi: set ft=yaml.jinja :
{% set mulegrpid = salt['environ.get']('MULE_GRPID') %}
{% set muledeploytype = salt['environ.get']('MULE_DEPLOY_TYPE') %}

{% if mulegrpid and muledeploytype == 'Folder' %}
copy_new_app_to_mulegrp:
 file.managed:
   - name: /opt/mule/apps/{{ salt['pillar.get']('newfile') }}
   - source: salt://mule/muleapp/{{ salt['pillar.get']('grpid') }}/{{ salt['pillar.get']('env') }}/{{ salt['pillar.get']('newfile') }}
   - template: jinja

{% endif %}