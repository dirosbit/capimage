copy_mule_domain:
 file.recurse:
   - name: /opt/mule/domains
   - source: salt://mule/shareddomains
   - clean: false
