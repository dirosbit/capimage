# vi: set ft=yaml.jinja :
{% set mulegrpid = salt['environ.get']('MULE_GRPID') %}
{% set muledeploytype = salt['environ.get']('MULE_DEPLOY_TYPE') %}
{% set muleenv = salt['environ.get']('MULE_ENV') %}

{% if mulegrpid and muledeploytype == 'Tag' %}
copy_new_app_to_mulegrp:
 file.managed:
   - name: /opt/mule/apps/{{ salt['pillar.get']('newfile') }}
   - source: salt://mule/muleapp/{{ salt['pillar.get']('grpid') }}/{{ salt['pillar.get']('newfile') }}
   - template: jinja
   - include_pat: E@((.*{{ muleenv }}.*)\.zip$)
{% endif %}