# vi: set ft=yaml.jinja :
{% set mulegrpid = salt['environ.get']('MULE_GRPID') %}
{% set muledeploytype = salt['environ.get']('MULE_DEPLOY_TYPE') %}
{% set muleenv = salt['environ.get']('MULE_ENV') %}

{% if mulegrpid and muledeploytype == 'Folder' %}

copy_ping_app:
 file.recurse:
   - name: /opt/mule/apps
   - source: salt://mule/monitorapp
   - template: jinja
   - clean: True
   - include_pat: E@zip
   - exclude_pat: E@(default)|(anchor)|(DS_Store)
   
copy_mule_app_by_folder:
 file.recurse:
   - name: /opt/mule/apps
   - source: salt://mule/muleapp/{{ mulegrpid }}/{{ muleenv }}
   - template: jinja
   - clean: True
   - include_pat: E@zip
   - exclude_pat: E@(default)|(anchor)|(ping)|(DS_Store)

{% endif %}

{% if mulegrpid and muledeploytype == 'Tag' %}

copy_ping_app:
 file.recurse:
   - name: /opt/mule/apps
   - source: salt://mule/monitorapp
   - template: jinja
   - clean: True
   - include_pat: E@zip
   - exclude_pat: E@(default)|(anchor)|(DS_Store)

copy_mule_app_by_tag:
 file.recurse:
   - name: /opt/mule/apps
   - source: salt://mule/muleapp/{{ mulegrpid }}
   - template: jinja
   - clean: True
   - include_pat: E@((.*{{ muleenv }}.*)\.zip$)
   - exclude_pat: E@(default)|(anchor)|(ping)|(DS_Store)
{% endif %}
