# vi: set ft=yaml.jinja :

{% set muleappchange = data['data']['change'] %}
{% set muleapppath = data['data']['path'] %}
{% set mulegrpid = muleapppath.split("/")[5] %}
{% set tokensix = muleapppath.split("/")[6] %}

{% if tokensix == "dev" or tokensix == "stage" or tokensix == "prod" %}
   {% set deploytype = "folder" %}
   {% set muleappname = muleapppath.split("/")[7] %} 
   {% set muleenv = muleapppath.split("/")[6] %}
{% else %}
   {% set deploytype = "tag" %}
   {% set muleappname = tokensix %}
   {% if "dev" in muleappname %}
     {% set muleenv = "dev" %}
   {% elif "stage" in muleappname %}
     {% set muleenv = "stage" %}
   {% elif "prod" in muleappname %}
     {% set muleenv = "prod" %}
   {% else %}
     {% set muleenv = "notsupported" %}
   {% endif %}
{% endif %}

{% if deploytype ==  "tag" %}
  {% if muleappchange == 'IN_CREATE' or muleappchange == 'IN_MODIFY' %}
copy_new_app_to_mulegrp:
  local.state.apply:
    - name: Tell Minion to Copy New File
    - tgt: 'G@grpid:{{ mulegrpid }} and G@env:{{ muleenv }}'
    - expr_form: compound
    - arg:
      - salt.mule.addnewapps-envbytags
      - "pillar={newfile: {{ muleappname }}, grpid: {{ mulegrpid }}}" 
  {% elif muleappchange == 'IN_DELETE' %}
    {% set muleappanchor = muleappname.replace(".zip","-anchor.txt") %}
delete_old_app_from_mulegrp:
  local.state.apply:
    - name: Tell Minion to Delete Old File
    - tgt: 'G@grpid:{{ mulegrpid }} and G@env:{{ muleenv }}'
    - expr_form: compound
    - arg:
      - salt.mule.deleteoldapps-envbytags
      - "pillar={oldfile: {{ muleappanchor }}, grpid: {{ mulegrpid }}}" 
  {% endif %}
{% elif deploytype == "folder" %}
  {% if muleappchange == 'IN_CREATE' or muleappchange == 'IN_MODIFY' %}
copy_new_app_to_mulegrp:
  local.state.apply:
    - name: Tell Minion to Copy New File
    - tgt: 'G@grpid:{{ mulegrpid }} and G@env:{{ muleenv }}'
    - expr_form: compound
    - arg:
      - salt.mule.addnewapps-envbyfolders
      - "pillar={newfile: {{ muleappname }}, grpid: {{ mulegrpid }}, env: {{ muleenv}} }" 
  {% elif muleappchange == 'IN_DELETE' %}
    {% set muleappanchor = muleappname.replace(".zip","-anchor.txt") %}
delete_old_app_from_mulegrp:
  local.state.apply:
    - name: Tell Minion to Delete Old File
    - tgt: 'G@grpid:{{ mulegrpid }} and G@env:{{ muleenv }}'
    - expr_form: compound
    - arg:
      - salt.mule.deleteoldapps-envbyfolders
      - "pillar={oldfile: {{ muleappanchor }}, grpid: {{ mulegrpid }}, env: {{ muleenv}}}"             
  {% endif %}
{% endif %}