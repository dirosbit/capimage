# vi: set ft=yaml.jinja :
{% set muleappchange = data['data']['change'] %}
{% set muleapppath = data['data']['path'] %}
{% set muleappname = muleapppath.split("/")[7] %}
{% set muleenv = muleapppath.split("/")[6] %}
{% set mulegrpid = muleapppath.split("/")[5] %}


{% if muleappchange == 'IN_CREATE' or muleappchange == 'IN_MODIFY' %}
copy_new_app_to_mulegrp:
  local.state.apply:
    - name: Tell Minion to Copy New File
    - tgt: 'G@grpid:{{ mulegrpid }} and G@env:{{ muleenv }}'
    - expr_form: compound
    - arg:
      - salt.mule.addnewapps-envbyfolders
      - "pillar={newfile: {{ muleappname }}, grpid: {{ mulegrpid }}, env: {{ muleenv}} }" 
      
      
{% elif muleappchange == 'IN_DELETE' %}

{% set muleappanchor = muleappname.replace(".zip","-anchor.txt") %}

delete_old_app_from_mulegrp:
  local.state.apply:
    - name: Tell Minion to Delete Old File
    - tgt: 'G@grpid:{{ mulegrpid }} and G@env:{{ muleenv }}'
    - expr_form: compound
    - arg:
      - salt.mule.deleteoldapps-envbyfolders
      - "pillar={oldfile: {{ muleappanchor }}, grpid: {{ mulegrpid }}, env: {{ muleenv}}}" 
            
{% endif %}
