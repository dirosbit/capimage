# vi: set ft=yaml.jinja :
{% set muleminionid = data['id'] %}

{% if 'mule' in data['id'] %}

{% set muleid = data['id'] %}
{% set mulegrpid = muleid.split("-")[1] %}
{% set muleenv = muleid.split("-")[2] %}

{% if data['fun'] == 'state.highstate' and data['cmd'] == '_return' and data['success']  %}

copy_domains_to_new_minion:
  local.state.apply:
    - name: Tell Minion to Copy Shared Domain
    - tgt: {{ muleid }}
    - arg:
      - salt.mule.copyfiles-domains
      
{% elif data['fun'] == 'state.apply' and data['fun_args'][0] == 'salt.mule.copyfiles-domains' and data['success'] %}

copy_apps_to_new_minion_by_folder:
  local.state.apply:
    - name: Tell Minion to Copy All Apps By Folder
    - tgt: '{{ muleid }} and G@env:{{ muleenv }}'
    - expr_form: compound
    - arg:
      - salt.mule.copyfiles-apps
      - "pillar={muleid: {{ muleid }}, grpid: {{ mulegrpid }}, env: {{ muleenv}} }" 

{% endif %}
{% endif %}