# vi: set ft=yaml.jinja :
{% set muleappchange = data['data']['change'] %}
{% set muleapppath = data['data']['path'] %}
{% set muleappname = muleapppath.split("/")[6] %}
{% set mulegrpid = muleapppath.split("/")[5] %}

{% if "dev" in muleappname %}
  {% set muleenv = "dev" %}
{% elif "stage" in muleappname %}
  {% set muleenv = "stage" %}
{% elif "prod" in muleappname %}
  {% set muleenv = "prod" %}
{% else %}
  {% set muleenv = "notsupported" %}
{% endif -%}

{% if muleappchange == 'IN_CREATE' or muleappchange == 'IN_MODIFY' %}

copy_new_app_to_mulegrp:
  local.state.apply:
    - name: Tell Minion to Copy New File
    - tgt: 'G@grpid:{{ mulegrpid }} and G@env:{{ muleenv }}'
    - expr_form: compound
    - arg:
      - salt.mule.addnewapps-envbytags
      - "pillar={newfile: {{ muleappname }}, grpid: {{ mulegrpid }}}" 
      
      
{% elif muleappchange == 'IN_DELETE' %}

{% set muleappanchor = muleappname.replace(".zip","-anchor.txt") %}

delete_old_app_from_mulegrp:
  local.state.apply:
    - name: Tell Minion to Delete Old File
    - tgt: 'G@grpid:{{ mulegrpid }} and G@env:{{ muleenv }}'
    - expr_form: compound
    - arg:
      - salt.mule.deleteoldapps-envbytags
      - "pillar={oldfile: {{ muleappanchor }}, grpid: {{ mulegrpid }}}" 
            
{% endif %}
