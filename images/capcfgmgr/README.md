Mule and Salt on CentOS - Docker Image
======================================

This image provides a Salt Master setup withits worker minion for triggering App Deployments. Salt Minion has a beacon for watching any new Deployments and triggres the master for distribution. Supervisord controls the services within this container. This image has been built to run within Rancher. 

Image includes both Salt-Master, Salt-Minion, Supervisord and provision to configure with Application Artifact location.

This is a companion project of Rancher Mule project which provides the walkthrough for how to use this image in a Rancher context.

## Environment variables

Env variables are used to set config on startup. All the required Environment fields are expected to be set by Rancher-Compose

 - ORCHESTRATOR      - DOCKER is the default in the Image. Its set to RANCHER by the Rancher Environment
 - HOSTNAME          - Captured from the Container Instance to uniquely identify the Salt Minion

## Volumes

Following paths can be mounted from the container. These have not been mounted in this image

 - 'var/log/supervisor' - Supervisor Log

All logs are routed to the Supervisor Log to ensure the Docker Logs can be captured in one location. This enables looking at all the logs without going through shell.

## Build

### Use the pre built image
The pre built image can be downloaded using docker directly. After that you do not need to use this command again, you will have the image on your local computer.

```
docker pull dirosden/saltmaster
```

### Build the docker image by yourself
If you prefer you can easily build the docker image by yourself. After this the image is ready for use on your machine and can be used for multiple starts.

```
git clone https://dirosbit@bitbucket.org/dirosbit/apiplatform.git
cd images/saltmaster
make clean
make build
```

##  Key files to change
Following are the files to change to fit your environment:
 - /saltmaster/config/init/configureenv.sh - The Start Script configures the Image with the Salt Master ID. If any modifications are needed, this will be the place to go to.