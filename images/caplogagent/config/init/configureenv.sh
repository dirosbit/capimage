#!/bin/bash
    
# Variables from environement
: ${LOG_LEVEL:=info}
: ${ORCHESTRATOR:=DOCKER}
: ${HOSTNAME:=localhost}
: ${OPTIONS:=}

#Change the Runtime specific components

if [ $ORCHESTRATOR == "RANCHER" ]; then
   echo "INFO: Configuring Logagent for Rancher"
else
    echo "INFO: No Orchestrator. Can only be used for confirming if Container starts properly"
    export HOSTNAME=localhost
fi
    
