Logagent
==============

This image is the Logagent with Dynamic Configuration of Kafka based on Rancher Metadata service. It is derived from
- [logspout-kafka][logspout-kafka]
- [rancher-gen][rancher-gen]

## Usage

This image runs the Logagent that connects to the Kafka bus publishing CAP messages to capkafkalogs topic and Config Manager messages to cfgmgrkafkalogs


[logspout-kafka]: https://github.com/gettyimages/logspout-kafka
[rancher-gen]: https://github.com/janeczku/go-rancher-gen