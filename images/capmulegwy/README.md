Mule and Salt on CentOS - Docker Image
======================================

This image provides a Mule Community edition with salt minion setup. Salt Minion manages the App Deployment to the Mule instance and making any other configuration changes. Supervisord controls the services within this container. This image has been built to run within Rancher. 

Image includes Mule 3.8 Community Edition, Salt-Minion and Supervisord. Dockerfile has provision to upgrade to Enterprise Edition, configure with Mulesoft's Anypoint Platform and provide license files.

This is a companion project of Rancher Mule project which provides the walkthrough for how to use this image in a Rancher context.

## Environment variables

Env variables are used to set config on startup. All the required Environment fields are expected to be set by Rancher-Compose

 - SALT_MASTER       - The Container Reference for the Salt Master. Will be set by Rancher
 - MULE_ENV          - The Environment of this Mule Instance. Captured as an input on the Rancher Catalog
 - MULE_DEPLOY_TYPE  - The Packaging approach for this Mule Instance. Folder/Tag. Captured as an input on the Rancher Catalog
 - MULE_CLIENTID     - Anypoint Platform Client ID for the Organization that owns this instance
 - MULE_CLIENTSECRET - Anypoint Platform Client Secret for the Organization that owns this instance
 - ORCHESTRATOR      - DOCKER is the default in the Image. Its set to RANCHER by the Rancher Environment
 - HOSTNAME          - Captured from the Container Instance to uniquely identify the Salt Minion

## Volumes

Following paths can be mounted from the container. These have not been mounted in this image

 - 'var/log/supervisor' - Supervisor Log

All logs are routed to the Supervisor Log to ensure the Docker Logs can be captured in one location. This enables looking at all the logs without going through shell.

## Build

### Use the pre built image
The pre built image can be downloaded using docker directly. After that you do not need to use this command again, you will have the image on your local computer.

```
docker pull dirosden/capruntime
```

### Build the docker image by yourself
If you prefer you can easily build the docker image by yourself. After this the image is ready for use on your machine and can be used for multiple starts.

```
git clone https://dirosbit@bitbucket.org/dirosbit/cap.git
cd images/capmulegwy
make clean
make build
```

##  Key files to change
Following are the files to change to fit your environment:
 - /capmulegwy/Dockerfile - Section that has been marked for Enterprise users can be uncommented and proper credentials provided
 - /capmulegwy/srv/mule/muleLicenseKey.lic - Replace with your enterprise License. If not provided, the Image will be restricted by Mule's trial license period
 - /capmulegwy/config/init/configureenv.sh - The Start Script configures the Image with your Org Client ID, Secret, Mule Environment. If any modifications are needed, this will be the place to go to.