#!/bin/bash
    
# Variables from environement
: ${SALT_MASTER:=localhost}
: ${SALT_USE:=minion}
: ${LOG_LEVEL:=info}
: ${MULE_ENV:=dev}
: ${MULE_DEPLOY_TYPE:=FOLDER}
: ${MULE_CLIENTID:=}
: ${MULE_CLIENTSECRET:=}
: ${MULE_GRPID:=}
: ${MULE_LISTEN_AT:=}
: ${ORCHESTRATOR:=DOCKER}
: ${HOSTNAME:=localhost}
: ${OPTIONS:=}

#Change the Runtime specific components
#    - Salt Minion  : Remove earlier master keys, copy default config, change master list
#    - Mule Wrapper : Change the Mule Wrapper conf values

rm -f /etc/salt/minion
cp /etc/salt/minion-default /etc/salt/minion

rm -f /opt/mule/conf/wrapper.conf
cp /etc/mule/wrapper-default.conf /opt/mule/conf/wrapper.conf

saltmstr=""
if [ $ORCHESTRATOR == "RANCHER" ]; then
   echo "INFO: Configuring Salt Minion and Mule"
   stackname=$(curl -s rancher-metadata.rancher.internal/latest/self/stack/name)
   export MULE_STKNAME=$stackname

   for c in $(curl -s rancher-metadata.rancher.internal/latest/services/ | grep "grandmaster"); do
     i=$(echo $c | cut -d'=' -f1)
     saltmstr+="\n"
     saltmstr+=$(echo "  - grandmaster.$(curl -s rancher-metadata.rancher.internal/latest/services/$i/stack_name)")
   done
   sed -i 's/master: 127.0.0.1/master: '"$saltmstr"'/' /etc/salt/minion
   sed -i 's/grpid=default/grpid='"$MULE_GRPID"'/' /etc/salt/minion
else
    echo "INFO: No Orchestrator. Can only be used for confirming if Container starts properly"
    export MULE_GRPID=grndmstrminion
    export HOSTNAME=localhost
fi
    
sed -i 's/%-5p %d \[%t\] %c: %m%n/time=%d level=%-8p service=mule grpid='"$MULE_GRPID"' package=\[%t\] function=%c: message=%m%n/' /opt/mule/conf/log4j2.xml

if [ $MULE_CLIENTID and $MULE_CLIENTSECRET ]; then
  echo "INFO: Setting Client ID and Secret to bind to Anypoint Platform"
  sed -i 's/# wrapper.java.additional.<n>=-Danypoint.platform.client_id=XXXXXXXX/wrapper.java.additional.100=-Danypoint.platform.client_id='"$MULE_CLIENTID"'/' /opt/mule/conf/wrapper.conf
  sed -i 's/# wrapper.java.additional.<n>=-Danypoint.platform.client_secret=XXXXXXXX/wrapper.java.additional.101=-Danypoint.platform.client_secret='"$MULE_CLIENTSECRET"'/' /opt/mule/conf/wrapper.conf  
fi

# Use contiguous numbering for Mule CE Additional Params. New ones start at 22.

echo 'wrapper.java.additional.22=-Dmule.env='$MULE_ENV >> /opt/mule/conf/wrapper.conf

# Set minion id
echo mule-$MULE_GRPID-$MULE_ENV-$HOSTNAME > /etc/salt/minion_id
